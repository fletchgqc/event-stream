const assert = require('assert');
const StringifyTransform = require('../lib/StringifyTransform');

describe('StringifyTransform:', () => {
  it('should return messages as JSON', done => {
    const stream = new StringifyTransform();
    stream.on('data', data => {
      assert.equal(data, '{"some":"object"}');
      done();
    });
    stream.write({some: 'object'});
  });
});
const assert = require('assert');
const sinon = require('sinon');
const EventTracingStream = require('../lib/EventTracingStream');
let logger;
let stream;

describe('EventTracingStream:', () => {
  beforeEach(
    () => {
      logger = {trace: sinon.spy()};
      stream = new EventTracingStream(logger);
    });
  it('should log an event',
    done => {
      let event = {event: 'test', value: {some: 'property'}};
      stream.on('data', () => {
        assert.deepEqual(logger.trace.args[0],
          ['Event: %s - Payload: %j', event.event, event.value]);
        done();
      });
      stream.write(event);
    });

  it('should not log any other messages',
    done => {
      let nonevent = {some: 'property'};
      stream.on('data', item => {
        assert.equal(nonevent, item, 'Original message should not be modified');
        assert.equal(logger.trace.args.length, 0);
        done();
      });
      stream.write(nonevent);
    });

  it('should pass messages through',
    done => {
      let event = {event: 'test', value: {some: 'property'}};
      stream.on('data', item => {
        assert.equal(event, item, 'Original message should not be modified');
        done();
      });
      stream.write(event);
    });
});
const stream = require('stream');

class EventTracingStream extends stream.Transform {
  constructor(_logger) {
    super();
    this._readableState.objectMode = true;
    this._writableState.objectMode = true;
    this.logger = _logger;
  }

  //noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
  _transform(obj, encoding, cb) {
    if (obj.event && obj.value) {
      this.logger.trace('Event: %s - Payload: %j', obj.event, obj.value);
    }
    this.push(obj);
    cb();
  }
}

module.exports = EventTracingStream;